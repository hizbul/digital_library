version: "3.5"

services:
  db:
    image: "postgres:alpine"
    env_file:
      - ./backend/.env.prod.db
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -U postgres"]
      interval: 2s
      timeout: 2s
      retries: 10
    depends_on:
      - traefik
    ports:
      - "5433:5432"
    volumes:
      - dbdata:/var/lib/postgresql/data:delegated

  broker:
    image: "rabbitmq:alpine"

  result:
    image: "redis:alpine"
    ports:
      - "6379:6379"

  frontend:
    image: "${FRONTEND_IMAGE}"
    build:
      context: ./frontend
      dockerfile: Dockerfile.prod
      cache_from:
        - "${FRONTEND_IMAGE}"
    env_file:
      - ./frontend/.env
    volumes:
      - ./frontend:/app
      - /app/node_modules
      - /app/.next
    ports:
      - "3000:3000"
    labels:
      - "traefik.http.routers.digital-library.rule=Host(`digital-library.com`)"
    depends_on:
      - backend
      - traefik

  backend:
    image: "${BACKEND_IMAGE}"
    build:
      context: .
      dockerfile: backend/Dockerfile.prod
      cache_from:
        - "${BACKEND_IMAGE}"
    ports:
      - "8000:8000"
    command: >
      sh -c "sh migrations.sh && gunicorn config.wsgi --log-file - -b 0.0.0.0 --reload"
    env_file: backend/.env
    volumes:
      - ./:/home/user/app/
      - static_volume:/home/user/app/backend/staticfiles
      - media_volume:/home/user/app/backend/mediafiles
    labels:
      - "traefik.http.routers.digital-library.rule=Host(`digital-library.com`)"
    depends_on:
      - broker
      - result
      - db
      - traefik

  celery:
    build:
      context: .
      dockerfile: backend/Dockerfile.prod
    command: celery --app=config worker --loglevel=info
    volumes:
      - ./:/home/user/app/
    env_file: backend/.env
    depends_on:
      - db
      - broker
      - result
      - traefik

  traefik:
    image: traefik:v3.0.0-beta5
    command: --api.insecure=true --providers.docker
    ports:
      - "80:80"
      - "8080:8080"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock

volumes:
  static_volume:
  media_volume:
  dbdata:
    external: true
    name: digital_library_dbdata
