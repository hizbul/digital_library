# common/middlewares.py

from django.conf import settings
from django.http import HttpResponseForbidden


class ApiHeaderCheckMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response
        self.required_header = settings.API_HEADER

    def __call__(self, request):
        if request.path.startswith("/api/"):
            required_header_value = request.headers.get(
                'X-Api-Required-Header', '')

            # Allow authenticated users without checking the header
            if request.user.is_authenticated:
                return self.get_response(request)

            if settings.DJANGO_SETTINGS_MODULE == 'config.settings.test':
                return self.get_response(request)

            if self.required_header not in required_header_value:
                return HttpResponseForbidden("You don't have access right!")

        response = self.get_response(request)
        return response
