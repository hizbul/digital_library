import random

from django.core.management.base import BaseCommand
from django.db import IntegrityError
from faker import Faker
from library.models import Author, Book

fake = Faker()


class Command(BaseCommand):
    help = 'Seed books with random data'

    def add_arguments(self, parser):
        parser.add_argument('--num_entries', type=int, nargs='?',
                            default=500, help='Number of entries to seed')
        parser.add_argument('--overwrite', action='store_true',
                            help='Overwrite existing books')

    def handle(self, *args, **options):
        num_entries = options['num_entries']
        overwrite = options['overwrite']

        if overwrite:
            self.stdout.write(self.style.WARNING("Overwriting Books"))
            Book.objects.all().delete()

        authors = Author.objects.all()
        books = []

        for _ in range(num_entries):
            num_authors = random.choice([1, 1, 1, 1, 2])
            selected_authors = random.sample(
                list(authors), min(num_authors, len(authors)))

            try:
                book = Book.objects.create(
                    title=fake.sentence(nb_words=8),
                    publication_year=random.choice(range(1900, 2024)),
                    summary=fake.text()
                )
                book.authors.set(selected_authors)

                books.append(book)
            except IntegrityError as e:
                print(e)

        self.stdout.write(self.style.SUCCESS(
            f'{num_entries} Books added'))
