from django.core.management.base import BaseCommand
from faker import Faker
from library.models import Author

fake = Faker()


class Command(BaseCommand):
    help = 'Seed authors with random data'

    def add_arguments(self, parser):
        parser.add_argument('--num_entries', type=int, nargs='?',
                            default=100, help='Number of entries to seed')
        parser.add_argument('--overwrite', action='store_true',
                            help='Overwrite existing authors')

    def handle(self, *args, **options):
        num_entries = options['num_entries']

        overwrite = options['overwrite']

        if overwrite:
            self.stdout.write(self.style.WARNING("Overwriting Authors"))
            Author.objects.all().delete()

        Author.objects.bulk_create(
            [Author(name=fake.name(), bio=fake.sentence()) for _ in range(num_entries)])

        self.stdout.write(self.style.SUCCESS(
            f'{num_entries} Authors added'))
