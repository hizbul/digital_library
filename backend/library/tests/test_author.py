import pytest
from django.core.exceptions import ValidationError
from faker import Faker
from library.models import Author

faker = Faker()


@pytest.mark.django_db
def test_author_str_representation():
    author = Author(name="Nameera Bahar", bio="Daughter of Hizbul Bahar")
    assert str(author) == "Nameera Bahar"


@pytest.mark.django_db
def test_author_name_required():
    author = Author(bio="Daughter of Hizbul Bahar")
    with pytest.raises(ValidationError) as ex:
        author.full_clean()

    expected_error_message = {'name': ['This field cannot be blank.']}
    assert str(ex.value.message_dict) == str(expected_error_message)


@pytest.mark.django_db
def setup_authore_test():
    number_of_author = 5
    Author.objects.bulk_create(
        [Author(name=faker.name, bio=faker.text()) for _ in range(number_of_author)])
    assert Author.objects.count() == number_of_author


@pytest.mark.django_db
def test_author_create():
    author = Author.objects.create(name='Nameera Bahar', bio=faker.text())
    assert author.name == 'Nameera Bahar'


@pytest.mark.django_db
def test_author_update():
    author = Author.objects.create(name='Nameera Bahar', bio=faker.text())
    assert author.name == 'Nameera Bahar'
    author.name = 'Hizbul Bahar'
    author.save()
    assert author.name == 'Hizbul Bahar'


@pytest.mark.django_db
def test_author_delete():
    author = Author.objects.create(name='Nameera Bahar', bio=faker.text())
    assert author.name == 'Nameera Bahar'
    id = author.id
    author.delete()
    with pytest.raises(Author.DoesNotExist) as ex:
        Author.objects.get(pk=id)
    assert str(ex.value) == 'Author matching query does not exist.'
