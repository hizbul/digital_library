import random

import pytest
from decouple import config
from django.contrib.postgres.search import SearchQuery, SearchVector
from django.core.exceptions import ValidationError
from django.db import transaction
from faker import Faker
from library.models import Author, Book, update_search_vector_task
from rest_framework import status
from rest_framework.test import APIClient

faker = Faker()
BASE_URL = f"{config('BASE_URL')}/api/v1"


@pytest.mark.django_db
def test_book_str_representation():
    book = Book(title="Sample Title",
                publication_year=2022, summary="A summary")
    assert str(book) == "Sample Title"


@pytest.mark.django_db
def test_invalid_publication_year():
    Author.objects.create(name="Author1")
    invalid_year = 999
    book = Book(title="Invalid Year Book",
                publication_year=invalid_year, summary="A summary")
    with pytest.raises(ValidationError):
        book.full_clean()


@pytest.mark.django_db
def test_search_vector_update_on_create():
    author = Author.objects.create(name="Author1")
    book = Book.objects.create(
        title="Search Vector Book", publication_year=2022, summary="A summary")
    book.authors.add(author)
    book.refresh_from_db()
    assert book.search_vector is not None


@pytest.mark.django_db
def test_search_vector_update_on_update():
    author = Author.objects.create(name="Author1")
    book = Book.objects.create(
        title="Search Vector Book", publication_year=2022, summary="A summary")
    book.authors.add(author)

    # Ensure search_vector is initially set
    initial_search_vector = book.search_vector

    # Update the book
    book.title = "Updated Title"
    with transaction.atomic():
        book.save()
        transaction.on_commit(lambda: update_search_vector_task(book.id))
    # Ensure search_vector is updated
    updated_search_vector = Book.objects.get(pk=book.pk).search_vector
    assert initial_search_vector != updated_search_vector


@pytest.fixture
@pytest.mark.django_db
def setup_fulltext_search_test():
    author = Author.objects.create(name='Nameera Bahar')

    book1 = Book.objects.create(
        title='Learn Python the hard way',
        publication_year=random.choice([2019, 2020, 2021, 2022, 2023, 2024]),
        summary=faker.text()
    )
    book1.authors.add(author)

    book2 = Book.objects.create(
        title='Learn Django the easy way',
        publication_year=random.choice([2019, 2020, 2021, 2022, 2023, 2024]),
        summary=faker.text()
    )
    book2.authors.add(author)

    book3 = Book.objects.create(
        title='Another book test case',
        publication_year=random.choice([2019, 2020, 2021, 2022, 2023, 2024]),
        summary=faker.text()
    )
    book3.authors.add(author)

    return author, [book1, book2, book3]


@pytest.mark.django_db
def test_full_text_search(setup_fulltext_search_test):
    author, books = setup_fulltext_search_test
    search_query = SearchQuery("Python")
    search_results = Book.objects.annotate(
        search=SearchVector('title', 'summary', 'authors__name')
    ).filter(search=search_query).values_list('title', 'summary', 'authors__name')

    for result in search_results:
        assert "Python" in result[0]
        assert "Python" not in result[1]
        assert "Python" not in result[2]


@pytest.mark.django_db
def test_full_text_search_count_book(setup_fulltext_search_test):
    author, books = setup_fulltext_search_test
    search_query = SearchQuery("Learn way")
    search_results = Book.objects.annotate(
        search=SearchVector('title', 'summary', 'authors__name')
    ).filter(search=search_query).values_list('title', 'summary', 'authors__name')
    assert len(search_results) == 2


@pytest.fixture
def api_client():
    return APIClient()


@pytest.fixture
def api_create_book(api_client):
    author_response = api_client.post(f'{BASE_URL}/authors/', {
        'name': 'John Doe',
        'bio': 'A bio for John Doe'
    })

    assert author_response.status_code == 201
    author_id = author_response.json()['id']

    # Create a book with the created author
    book_response = api_client.post(f'{BASE_URL}/books/', {
        'title': 'Test book one',
        'publication_year': 2019,
        'summary': 'This is a test summary, can not write more here.',
        'authors': [author_id]
    })
    return book_response


@pytest.mark.django_db
def test_create_author_and_book(api_create_book):
    book_response = api_create_book
    assert book_response.status_code == status.HTTP_201_CREATED

    book_data = book_response.json()
    assert book_data['title'] == 'Test book one'
    assert book_data['publication_year'] == 2019
    assert book_data['summary'] == 'This is a test summary, can not write more here.'
    assert book_data['authors'][0]['name'] == 'John Doe'


@pytest.mark.django_db
def test_book_search(api_client, setup_fulltext_search_test):
    search_response = api_client.get(f'{BASE_URL}/books/', {'q': 'Django'})
    assert search_response.status_code == status.HTTP_200_OK
    book_data = search_response.json()

    assert book_data['count'] == 1


@pytest.mark.django_db
def test_update_book(api_client, setup_fulltext_search_test):
    search_response = api_client.get(f'{BASE_URL}/books/', {'q': 'Django'})
    assert search_response.status_code == status.HTTP_200_OK
    search_data = search_response.json()
    assert 'results' in search_data and search_data['results']
    book_slug = search_data['results'][0]['slug']

    update_response = api_client.put(
        f"{BASE_URL}/books/{book_slug}/", {
            'title': 'two scoops of django 3.x',
            'publication_year': search_data['results'][0]['publication_year'],
            'summary': search_data['results'][0]['summary'],
            'authors': [author.get('id') for author in search_data['results'][0]['authors']]
        })
    assert update_response.status_code == status.HTTP_200_OK
    updated_response = api_client.get(f"{BASE_URL}/books/{book_slug}/")
    assert updated_response.status_code == status.HTTP_200_OK
    updated_book_data = updated_response.json()

    assert updated_book_data['title'] == 'two scoops of django 3.x'

    # check slug not updated
    assert updated_book_data['slug'] == book_slug


@pytest.mark.django_db
def test_delete_book(api_client, setup_fulltext_search_test):
    search_response = api_client.get(f'{BASE_URL}/books/', {'q': 'Django'})
    assert search_response.status_code == status.HTTP_200_OK
    search_data = search_response.json()
    assert 'results' in search_data and search_data['results']
    book_slug = search_data['results'][0]['slug']

    delete_response = api_client.delete(
        f"{BASE_URL}/books/{book_slug}/")
    assert delete_response.status_code == status.HTTP_204_NO_CONTENT
