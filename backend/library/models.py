import datetime

from autoslug import AutoSlugField
from celery import shared_task
from common.models import TimeStampedModel
from django.conf import settings
from django.contrib.postgres.indexes import GinIndex
from django.contrib.postgres.search import SearchVector, SearchVectorField
from django.core.validators import (MaxValueValidator, MinValueValidator,
                                    RegexValidator)
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver

current_year = datetime.datetime.now().year


class Author(TimeStampedModel):
    name = models.CharField(max_length=100, blank=False)
    bio = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']


class Book(TimeStampedModel):
    title = models.CharField(max_length=100)
    authors = models.ManyToManyField(Author)
    publication_year = models.PositiveIntegerField(
        validators=[
            MinValueValidator(
                1000, message='Year must be greater than or equal to 1000'),
            MaxValueValidator(
                current_year, message=f'Year must be less than or equal to {current_year}'),
            RegexValidator(
                regex=r'^[1-9]\d{3}$',
                message=f'Enter a valid 4-digit year (between 1000 and {current_year}).'
            )
        ]
    )
    summary = models.TextField()
    slug = AutoSlugField(populate_from='title', unique=True,
                         editable=False)
    search_vector = SearchVectorField(null=True, blank=True)

    def __str__(self):
        return self.title

    class Meta:
        indexes = [
            GinIndex(fields=["search_vector"]),
        ]


@shared_task
def update_search_vector_task(instance_id):
    instance = Book.objects.get(id=instance_id)
    update_search_vector_for_instance(instance)


def update_search_vector_for_instance(instance):
    author_names = ', '.join(author.name for author in instance.authors.all())
    search_vector = SearchVector('title', 'summary', models.Value(
        author_names, output_field=models.CharField()))

    # Update the search_vector in the database
    Book.objects.filter(id=instance.id).update(search_vector=search_vector)


@receiver(post_save, sender=Book)
def update_search_vector(sender, instance, created, **kwargs):
    if instance and instance.id:
        if settings.DJANGO_SETTINGS_MODULE == 'config.settings.test':
            update_search_vector_for_instance(instance)
        else:
            try:
                update_search_vector_task.delay(instance.id)
            except Exception:
                print("Connecting...")
