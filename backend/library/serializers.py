from rest_framework import serializers

from .models import Author, Book


class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = '__all__'


class BookSerializer(serializers.ModelSerializer):
    authors = serializers.PrimaryKeyRelatedField(
        queryset=Author.objects.all(), many=True)

    class Meta:
        model = Book
        lookup_field = 'slug'
        fields = '__all__'
        extra_kwargs = {
            'url': {'lookup_field': 'slug'}
        }

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['authors'] = AuthorSerializer(
            instance.authors.all(), many=True).data
        return data
