from django.contrib import admin

from .models import Author, Book

# Register your models here.


class AuthorAdmin(admin.ModelAdmin):
    list_display = ('name', 'bio')


class BookAdmin(admin.ModelAdmin):
    list_display = ('title', 'display_authors', 'publication_year', 'summary')
    readonly_fields = ('search_vector',)

    def display_authors(self, obj):
        return ', '.join(author.name for author in obj.authors.all())

    display_authors.short_description = 'Authors'  # Set a custom column header


admin.site.register(Book, BookAdmin)
admin.site.register(Author, AuthorAdmin)
