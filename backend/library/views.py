# library/views.py
from django.contrib.postgres.search import SearchQuery, SearchVector
from django.core.cache import cache
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django_filters import rest_framework as filters
from django_ratelimit.decorators import ratelimit
from rest_framework import viewsets

from .filters import BookFilter
from .models import Author, Book
from .serializers import AuthorSerializer, BookSerializer


class BookViewSet(viewsets.ModelViewSet):
    queryset = Book.objects.all()
    serializer_class = BookSerializer
    filter_backends = [filters.DjangoFilterBackend]
    filterset_class = BookFilter
    lookup_field = 'slug'
    ordering = ['-created']

    def get_queryset(self):
        queryset = super().get_queryset()
        search_query = self.request.query_params.get('q', '')

        if search_query:
            vector = SearchVector('title', 'summary', 'authors__name')
            search_vector = SearchQuery(search_query)

            queryset = queryset.annotate(
                search=vector
            ).filter(
                search=search_vector
            ).order_by('-search')
        else:
            queryset = queryset.order_by('-created', 'id')

        return queryset

    @method_decorator(ratelimit(key='ip', rate='10/m', block=True))
    def create(self, request, *args, **kwargs):
        return super().create(request, *args, **kwargs)

    @method_decorator(ratelimit(key='ip', rate='10/m', block=True))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(ratelimit(key='ip', rate='2/m', block=True))
    def destroy(self, request, *args, **kwargs):
        return super().destroy(request, *args, **kwargs)


class AuthorViewSet(viewsets.ModelViewSet):
    queryset = Author.objects.all()
    serializer_class = AuthorSerializer
    pagination_class = None

    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)

    @method_decorator(ratelimit(key='ip', rate='5/m', block=True))
    def create(self, request, *args, **kwargs):
        response = super().create(request, *args, **kwargs)

        return response
