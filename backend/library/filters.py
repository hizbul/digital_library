# library/filters.py
from django.contrib.postgres.search import SearchVector
from django_filters import rest_framework as filters

from .models import Book


class BookFilter(filters.FilterSet):
    q = filters.CharFilter(method='perform_search', label='Search')

    class Meta:
        model = Book
        fields = []

    def perform_search(self, queryset, name, value):
        vector = SearchVector('title', 'summary', 'authors__name')
        return queryset.annotate(search=vector).filter(search=value)
