SHELL := /bin/bash # Use bash syntax
ARG := $(word 2, $(MAKECMDGOALS) )


# Commands for Docker version
install_project:
	@echo "Initializaing project...."
	@cp backend/config/settings/local.py.example backend/config/settings/local.py
	@cp backend/.env.example backend/.env
	@cp frontend/.env.example frontend/.env
	@cp .gitlab-ci.yml.example gitlab-ci.yml

	@echo "Project initialized successfully."
	@docker volume create digital_library_database
	@docker-compose build
	@echo "Docker setup complete."

	@echo "Starting Docker containers..."
	@docker-compose up -d
	@echo "Docker containers started."


	@echo "Running migrations"
	@docker-compose run --rm backend python manage.py makemigrations
	@docker-compose run --rm backend python manage.py migrate
	@echo "Creating superuser..."
	@docker-compose run --rm backend python manage.py create_superuser
	@echo "Do you want to seed authors and books? (yes/no)"
	@read answer; \
	if [ "$$answer" = "yes" ]; then \
		echo "Seeding authors..."; \
		docker-compose run --rm backend python manage.py author_seeder; \
		echo "Authors seeded successfully."; \
		echo "Seeding books..."; \
		docker-compose run --rm backend python manage.py book_seeder; \
		echo "Books seeded successfully."; \
	else \
		echo "Skipping seeders."; \
	fi

	@echo "User 'admin' created. Default password is 'password'"
	@echo "Service URLs:"
	@echo "Backend: http://backend.localhost"
	@echo "Frontend: http://frontend.localhost"



init_project:
	@cp backend/config/settings/local.py.example backend/config/settings/local.py
	@cp backend/.env.example backend/.env
	@cp frontend/.env.example frontend/.env
	@cp .gitlab-ci.yml.example gitlab-ci.yml

docker_setup:
	docker volume create digital_library_database
	docker-compose build

docker_test:
	@cp backend/.env.test backend/.env
	docker-compose run --rm backend pytest
	@cp backend/.env.example backend/.env


docker_up:
	docker-compose up -d

docker_update_dependencies:
	docker-compose down --volumes
	docker-compose up -d --build

docker_down:
	docker-compose down --volumes

docker_logs:
	docker-compose logs -f $(ARG)

docker_makemigrations:
	docker-compose run --rm backend python manage.py makemigrations

docker_migrate:
	docker-compose run --rm backend python manage.py migrate

docker_create_superuser:
	docker-compose run --rm backend python manage.py create_superuser

docker_seed_author:
	docker-compose run --rm backend python manage.py author_seeder

docker_seed_book:
	docker-compose run --rm backend python manage.py book_seeder