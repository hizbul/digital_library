# Digital Library Documentation

### Setup

-   Clone the project on your machine:
    -   Please make sure docker in running in your machine.
    -   In your preferred directory `git clone -b develop https://gitlab.com/hizbul/digital_library.git`
    -   Navigate into the project `cd digital_library`
    -   Single command installation: `make install_project`

### Step by step installation:

-   Open a new command line window and go to the project's directory
-   Prepare project :
    `make init_project`
-   Run the initial setup:
    `make docker_setup`
-   Run the project:
    `make docker_up`
-   Run migrations: `make docker_makemigrations && docker_migrate`
-   Run seeders:

    -   `make docker_create_superuser` Creates a superuser with the username: _admin_ and password: _password_. Use these credentials to log in to the admin panel, where you can view books, authors, and API endpoints.
    -   `make docker_seed_author` Creates 100 Authors
    -   `make docker_seed_book` Create 500 Books using previously created authors.

-   Access [Frontend](http://frontend.localhost) in your browser; the project should be running there.
    -   When you run make docker_up, various containers are spun up (frontend, backend, database, redis, celery, etc.), each running on a different port.
    -   The container with the Next.js app uses port 3000, but i am using reverse proxy using [traefik](https://traefik.io/). However, if you access it in your browser, the app will display a list of books.
    -   To install dependencies use: `make docker_update_dependencies`
    -   To chekc logs: `make docker_logs backend` or `make docker_logs frontend`
    -   To stop the container: `make docker_down`

## Tools

-   [Django as a backend framework](https://www.djangoproject.com/)
-   [Django REST Framework](https://www.django-rest-framework.org/)
-   [CORS HEADERS](https://pypi.org/project/django-cors-headers/)
-   [Pyest for testing](https://pytest-django.readthedocs.io/en/latest/index.html)
-   [Celery for background task queue](https://docs.celeryq.dev/en/stable/index.html)
-   [Redis for caching and broker](https://redis.io/)
-   [Gunicorn HTTP Server for UNIX](https://gunicorn.org/)
-   [Rate Limit](https://django-ratelimit.readthedocs.io/en/stable/)
-   [PostgreSQL Database Adapter](https://pypi.org/project/psycopg2-binary/)
-   [Faker for seeding data](https://faker.readthedocs.io/en/master/)
-   [Sentry for error and logging](https://pypi.org/project/sentry-sdk/)

### Testing

Backend: `make docker_test`

Frontend: Not implemented

## Production Deployment

### Setup

-   This setup example is based on digital ocean, create a new Droplet with [Docker pre-installed](https://marketplace.digitalocean.com/apps/docker):
-   Once the status of the droplet is active, SSH into the instance as root and update the default password when prompted.
-   Next, generate a new SSH key: `$ ssh-keygen -t rsa`
-   Save the key to /root/.ssh/id_rsa and don't set a password. This will generate a public and private key -- id_rsa and id_rsa.pub, respectively. To set up passwordless SSH login, copy the public key over to the authorized_keys file and set the proper permissions:

    ```
    $ cat ~/.ssh/id_rsa.pub

    $ vi ~/.ssh/authorized_keys

    $ chmod 600 ~/.ssh/authorized_keys

    $ chmod 600 ~/.ssh/id_rsa
    ```

-   Copy the contents of the private key: `cat ~/.ssh/id_rsa`
-   Set it as an environment variable on your local machine:

    ```export PRIVATE_KEY='-----BEGIN RSA PRIVATE KEY-----
    MIIEpAIBAAKCAQEA04up8hoqzS1+APIB0RhjXyObwHQnOzhAk5Bd7mhkSbPkyhP1
    ...
    iWlX9HNavcydATJc1f0DpzF0u4zY8PY24RVoW8vk+bJANPp1o2IAkeajCaF3w9nf
    q/SyqAWVmvwYuIhDiHDaV2A==
    -----END RSA PRIVATE KEY-----'
    ```

-   Add the key to the [ssh-agent](https://www.ssh.com/ssh/agent): ` ssh-add - <<< "${PRIVATE_KEY}"`
-   Then, create a new directory for the app: `ssh -o StrictHostKeyChecking=no root@<YOUR_INSTANCE_IP> mkdir /digital_library`

-   There is few variables in `./setup_env.sh`, please take a look and based on it update Environment variables from GitLab project's CI/CD settings (Settings > CI / CD > Variables).

-   Finally , Commit and push your code to `master` branch to trigger a new build. Once the build passes, navigate to the domain name you provided of your instance.

## Performance Consideration

### Scaling Django Backend

-   **Horizontal Scaling**
    Add multiple web servers and utilize load balancers to distribute traffic evenly across multiple Django application server instances.
    Deploy on multiple servers and balance the load using container orchestration tools like Docker Swarm or Kubernetes.
-   **Database Scaling**
    Using PostgreSQL as the backend database, which is highly scalable RDBMS. Could implement database sharding or partitioning.
    Also, consider database replication for read-heavy workloads.
-   **Caching**:
    -   Already have Redis service in our container. We can store Authors in caching and, after adding/updating an author, invalidate the old cache `key` and generate a new cache.
    -   Our Books pagination size is 20; we can store the first 20 books in the cache and leverage homepage loading. Obviously, invalidate the cache after adding a new book, as we are showing books ordered by DESC.
-   **Asynchronous Processing**
    Offload time-consuming tasks to background jobs using Celery task queue.
    -   Currently using _Celery_ for adding _search_vector_ after creating a book.
    -   Book and Author creation can be processed in the background.
    -   Book and Author updates can be processed in the background.

### Scaling Next.js Frontend

-   **Content Delivery Network (CDN)**
    Employ CDNs like Cloudflare for caching and distributing static assets.
    Use CDNs for Next.js pages to enhance global performance and reduce latency.

-   **Client-Side Rendering (CSR) vs. Server-Side Rendering (SSR)**
    Next.js offers both CSR and SSR; choose the rendering strategy based on use cases (CSR for dynamic content, SSR for SEO benefits). Even for instance, we may choose hybrid rendering for a mix of CSR and SSR. It will improve frontend content rendering performance significantly.
-   **Load Balancing**
    Distribute incoming traffic among multiple Next.js servers using load balancers.
    Consider deploying Next.js as a static site or using serverless functions for specific pages like the Homepage, where we list our books.

### Ensuring High Performance

-   **Code Optimization**
    Optimize Django and Next.js code using tools like Django Debug Toolbar or Webpack Bundle Analyzer.
    Minimize unnecessary database queries and use built-in query caching.
-   **Frontend Optimization**
    Compress static assets and optimize images.
    Implement lazy loading for images and ensure a responsive design.
-   **Monitoring and Logging**
    Implement monitoring tools for performance metrics and error rates.
    Set up logging to capture and analyze application logs for troubleshooting. We are already using [SENTRY](https://sentry.io/for/django/) to monitor error logs.
-   **API Rate Limit**
    Add a rate limit to API endpoints to prevent continuous API requests to sensitive endpoints.

### Ensuring Reliability

-   **Automated Testing**
    mplement comprehensive unit, integration, and end-to-end tests for reliability.
    Use testing frameworks like _pytest_ for Django/Python and Jest for Next.js.
-   **Redundancy**
    Design architecture with redundancy for high availability. Use multiple instances of critical components and services.
-   **Backup and Disaster Recovery**
    Regularly backup databases and critical data.
    Establish a robust disaster recovery plan with off-site backups and quick data restoration procedures.
-   **Security Measures**
    Should follow best practices for Django and Next.js applications. Should install authentic and reliable django/nextjs packages and keep update.
    Regularly update dependencies, apply security patches, and conduct security audits.

## Improvement Write-Up

### System Design

-   Microservices architecture could be a good choice for further improvement. In this case, [FastAPI](https://fastapi.tiangolo.com/) can play a significant role as it's a very high-performance framework like NodeJS and Go.
-   Currently, I am using PostgreSQL's Full Text search API, which is a good fit for small to medium-scale applications. For further improvement, we may select [Elastic](https://www.elastic.co/) search and can display results more rapidly.
-   I am adding search_vector for Full Text search based on Django Signal. It works like a hook behind the scenes as a background task. During book creation and update, I'm adding title, authors, summary fields data to the Index search_vector field. We can use PostgreSQL's default CREATE OR REPLACE TRIGGER by creating new migrations. It'll add fields value without adding any code.

### Code

-   Initially, I implemented frontend by directly accessing the API and stored values in the React State. Later, I learned Redux for a single point of data store and implemented it. Later, I found Redux Thunk and Redux Saga middleware libraries for interacting with REST API asynchronously. It seems to me it will enhance the work experience with API communication for POST and PUT requests.
-   In the current system, there is a search box for full-text search, and results appear after clicking on a button. There is a way to improve here by implementing typeahead search after typing at least 3 characters in the box. After clearing the search box and loading book data will improve user experience.
-   Protecting API endpoints from anonymous users, I'm passing a **header** with each request and checking it from the backend by a middleware. Implementing JWT auth will make it easier and more secure.

### Further enhancement with new features:

-   We may add images for each book; it'll make UI more beautiful and informative.
-   Add authentication constraint for adding a book.
-   Can use a barcode scanner to add a book in the system along with manual entry.
-   Can add bulk import options, where admin users can import `csv/xlsx` files with book information. In the background, we may process book and author data and create books and authors.
