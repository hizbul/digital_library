import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  authors: [],
  author: {
    name: "",
    bio: "",
  },
  loading: false,
  authorModalVisible: false,
  errors: {},
  isFormValid: false,
  authorOptions: [],
  selectedAuthorOptions: []
};

const authorsSlice = createSlice({
  name: "authors",
  initialState,
  reducers: {
    fetchAuthorSuccess: (state, action) => {
      state.loading = false;
      state.errors = {}
      state.authors = [...state.authors, ...action.payload];
    },
    addAuthorSuccess: (state, action) => {
      state.authors.push(action.payload);
      state.authorOptions.push({
        value: action.payload.id,
        label: action.payload.name
      })
      state.selectedAuthorOptions.push({
        value: action.payload.id,
        label: action.payload.name
      })
    },
    addAuthorFailure: (state, action) => {
      state.loading = false;
      state.errors = action.payload;
    },
    setIsFormValid: (state, action) => {
      state.isFormValid = action.payload;
    },
    setAuthorName: (state, action) => {
      state.author.name = action.payload;
    },
    setAuthorBio: (state, action) => {
      state.author.bio = action.payload;
    },
    setAuthorOptions: (state, action) => {
      state.authorOptions = action.payload;
    },
    setSelectedAuthorOptions: (state, action) => {
      state.selectedAuthorOptions = action.payload;
    },
    setAuthorModalVisible: (state, action) => {
      state.authorModalVisible = action.payload;
    },
  },
});

export const {
  addAuthorSuccess,
  addAuthorFailure,
  setIsFormValid,
  setAuthorName,
  setAuthorBio,
  fetchAuthorSuccess,
  setAuthorOptions,
  setSelectedAuthorOptions,
  setAuthorModalVisible
} = authorsSlice.actions;
export default authorsSlice.reducer;
