// booksActions.js
import { BASE_URL, HEADERS } from "@/utils/constants";
import { addAuthorFailure, addAuthorSuccess, fetchAuthorSuccess, setAuthorOptions } from './authorSlice';


export const addAuthor = (author) => async(dispatch, getState) => {
  try {
    const res = await fetch(`${BASE_URL}/authors/`, {
      method: "POST",
        headers: {
          "Content-Type": "application/json",
          ...HEADERS
        },
      body: JSON.stringify(author)
    });
    if (res.status === 201) {
      const result = await res.json()
      dispatch(addAuthorSuccess(result))
    }
  } catch (error) {
    console.error(error);
    dispatch(addAuthorFailure(error.message));
  }
}


export const fectAuthors = () => async(dispatch, getState) => {
  try {
    const res = await fetch(`${BASE_URL}/authors/`, {
      headers: HEADERS
    });
    const response = await res.json();

    if (!res.ok) {
      dispatch(addAuthorFailure({message: 'Failed to fetch data'}))
    }

    const result = response.map((item) => ({
      value: item.id,
      label: item.name,
    }));
    dispatch(fetchAuthorSuccess(response))
    dispatch(setAuthorOptions(result))
  } catch (error) {
    dispatch(addAuthorFailure({message: 'Error fetching data:', error}))
    dispatch(fetchAuthorSuccess([]))
    dispatch(setAuthorOptions([]))
  }
}
