// booksActions.js
import { BASE_URL, HEADERS } from "@/utils/constants";
import { setSelectedAuthorOptions } from "../authors/authorSlice";
import {
  addBookBookSuccess,
  addBookFailure,
  fetchBooksFailure,
  fetchBooksStart,
  fetchBooksSuccess,
  removeBook,
  setErrors,
  setHasMoreData,
  setUpdateAbleBook,
  updateBookSuccess
} from "./booksSlice";


export const fetchBooks = (queryParams) => async (dispatch, getState) => {
  const { hasMoreData } = getState().books;

  if (!hasMoreData) {
    return;
  }

  dispatch(fetchBooksStart());

  try {
    const response = await fetch(`${BASE_URL}/books/${queryParams}`, {
      headers: HEADERS,
    });
    const res = await response.json();
    dispatch(fetchBooksSuccess(res.results));

    if (res.next === null) {
      dispatch(setHasMoreData(false));
    }
  } catch (error) {
    dispatch(fetchBooksFailure(error.message));
  }
};

export const deleteBook = (book) => async (dispatch, getState) => {
  try {
    const res = await fetch(`${BASE_URL}/books/${book.slug}/`, {
      method: "DELETE",
      headers: HEADERS,
    });
    if (res.status === 204) {
      dispatch(removeBook(book));
      return true
    }
    if (res.status === 403) {
      dispatch(fetchBooksFailure("Too many request!!"));
    }
  } catch (error) {
    dispatch(fetchBooksFailure(error.message));
  }
};


export const createBook = () => async (dispatch, getState) => {
  const { book } = getState().books;
  try {
    const res = await fetch(`${BASE_URL}/books/`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        ...HEADERS,
      },
      body: JSON.stringify(book),
    });
    if (res.status === 400) {
      const errorResponse = await res.json();
      dispatch(setErrors(errorResponse));
    }
    if (res.status === 201) {
      const objBook = await res.json();
      dispatch(addBookBookSuccess(objBook));
      return objBook
    }
  } catch (error) {
    dispatch(addBookFailure(error.message));
  }
};



export const getBookBySlug = (slug) => async (dispatch, getState) => {
  try {
    const res = await fetch(`${BASE_URL}/books/${slug}/`, {
      headers: HEADERS
    });
    if (res.status === 404) {
      dispatch(addBookFailure("Book not found"));
    }
    if (res.status === 200) {
      const response = await res.json();
      const selectedAuthors = response.authors.map((author) => ({
        value: author.id,
        label: author.name,
      }));
      dispatch(setUpdateAbleBook(response))
      dispatch(setSelectedAuthorOptions(selectedAuthors));
    }
  } catch (error) {
    dispatch(addBookFailure("Error fetching book:", error));
  }
};


export const updateBook = (book, slug) => async (dispatch, getState) => {
  try {
    const authors = book.authors.map(author => author.id)
    const res = await fetch(`${BASE_URL}/books/${slug}/`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
        ...HEADERS,
      },
      body: JSON.stringify({
        title: book.title,
        summary: book.summary,
        publication_year: book.publication_year,
        authors: authors
      }),
    });
    if (res.status === 200) {
      const result = await res.json()
      dispatch(updateBookSuccess(result))
      return result
    }

    if (res.status === 400) {
      let errorResponse;
      try {
        errorResponse = await res.json();
      } catch (error) {
        dispatch(addBookFailure("Invalid request."));
      }
      dispatch(setErrors(errorResponse));
    }
  } catch (error) {
    dispatch(addBookFailure(error.message));
  }
};