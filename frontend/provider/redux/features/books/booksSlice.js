import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  books: [],
  book: {
    title: "",
    publication_year: "",
    summary: "",
    authors: []
  },
  updatedBook: {
    title: "",
    publication_year: "",
    summary: "",
    authors: []
  },
  loading: false,
  error: null,
  errors: {},
  page: 1,
  hasMoreData: true,
  searchInput: "",
  showDeleteConfirmation: false,
  isDeleted: false,
  bookToBeDeleted: {},
  isFormValid: false,
  bookCreateSuccess: false,
  bookUpdatedSuccess: false
};

const booksSlice = createSlice({
  name: "books",
  initialState,
  reducers: {
    fetchBooksStart: (state) => {
      state.loading = true;
      state.error = null;
    },
    fetchBooksSuccess: (state, action) => {
      state.loading = false;
      const newBooks = action.payload.filter((newBook) => {
        return !state.books.some(
          (existingBook) => existingBook.id === newBook.id
        );
      });

      state.books = [...state.books, ...newBooks];
    },
    fetchBooksFailure: (state, action) => {
      state.loading = false;
      state.error = action.payload;
    },
    setHasMoreData: (state, action) => {
      state.hasMoreData = action.payload;
    },
    setNextPage: (state, action) => {
      state.page += 1;
    },
    setSearchInput: (state, action) => {
      state.searchInput = action.payload;
    },
    resetBooks: (state, action) => {
      state.books = [];
    },
    resetBook: (state, action) => {
      state.book = {
        title: "",
        publication_year: "",
        summary: "",
      }
    },
    removeBook: (state, action) => {
      state.books = state.books.filter((book) => book.id !== action.payload.id);
      state.isDeleted = true
    },
    setShowDeleteConfirmation: (state, action) => {
      state.showDeleteConfirmation = action.payload;
    },
    setDeletedBook: (state, action) => {
      state.bookToBeDeleted = action.payload
    },
    addBookBookSuccess: (state, action) => {
      const newBook = action.payload;
      const insertIndex = state.books.findIndex(book => book.id > newBook.id);
      if (insertIndex === -1) {
        state.books.push(newBook);
      } else {
        state.books.splice(insertIndex, 0, newBook);
      }
      state.books = [...state.books];
      state.bookCreateSuccess = true;
    },
    addBookFailure: (state, action) => {
      state.loading = false
      state.error = action.payload
    },
    setBook: (state, action) => {
      state.book = action.payload
    },
    setIsFormValid: (state, action) => {
      state.isFormValid = action.payload
    },
    setErrors: (state, action) => {
      state.errors = action.payload
    },
    updateBookSuccess: (state, action) => {
      state.books.map((book) => (book.id === action.payload.id ? {...book, ...action.payload} : book))
      state.error = null
      state.loading = false
      state.bookUpdatedSuccess = true
    },
    setUpdateAbleBook: (state, action) => {
      state.updatedBook = action.payload
    }
  },
});

export const {
  fetchBooksStart,
  fetchBooksSuccess,
  fetchBooksFailure,
  setHasMoreData,
  setNextPage,
  setSearchInput,
  resetBooks,
  removeBook,
  setShowDeleteConfirmation,
  setDeletedBook,
  addBookBookSuccess,
  addBookFailure,
  setBook,
  setIsFormValid,
  setErrors,
  updateBookSuccess,
  setUpdateAbleBook,
  resetBook
} = booksSlice.actions;
export default booksSlice.reducer;
