import { configureStore } from '@reduxjs/toolkit'
import authorReducer from './features/authors/authorSlice'
import bookReducer from './features/books/booksSlice'



export const store = configureStore({
  reducer: {
    books: bookReducer,
    authors: authorReducer
  },
})