import { Inter } from "next/font/google";
import { ToastContainer } from 'react-toastify';
import "./globals.css";

import StoreProvider from "@/provider/redux/storeProvider";
import 'react-toastify/dist/ReactToastify.css';

const inter = Inter({ subsets: ["latin"] });

export const metadata = {
  title: "Digital Library",
  description: "A Very Simple Digital Library",
};

export default function RootLayout({ children }) {
  return (
    <StoreProvider>
      <html lang="en" data-theme="winter">
      <body className={inter.className}>
        <main className="max-w-7xl h-full mx-auto my-16 rounded-md bg-white border-t-2 border-gray-200 shadow-md overflow-hidden">
          {children}
        </main>
        <ToastContainer />
      </body>
    </html>
    </StoreProvider>
  );
}
