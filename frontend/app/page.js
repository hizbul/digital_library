"use client";
import { setSearchInput } from "@/provider/redux/features/books/booksSlice";
import Link from "next/link";
import { useState } from "react";
import { FaPlus, FaSearch } from "react-icons/fa";
import { useDispatch } from 'react-redux';
import BookList from "./components/BookList";
import { Header } from "./components/Header";

export default function Home() {
  const dispatch = useDispatch()

  const [searchQuery, setSearchQuery] = useState("")

  const handleSearch = () => {
    dispatch(setSearchInput(searchQuery))
  }

  return (
    <>
      <Header>
        <input
          type="text"
          placeholder="Type here"
          className="input input-bordered rounded-r-none"
          value={searchQuery}
          onChange={(e) => setSearchQuery(e.target.value)}
        />
        <button
          className="btn btn-primary rounded-l-none"
          onClick={handleSearch}
        >
          <FaSearch />
          <span>Search</span>
        </button>
        <Link className="btn btn-primary ml-2" href={"/books/create"}>
          <span className="flex">
            <FaPlus /> Add Book
          </span>
        </Link>
      </Header>
      <BookList/>
    </>
  );
}
