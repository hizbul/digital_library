"use client";
import { Header } from "@/app/components/Header";
import Loader from "@/app/components/Loader";
import { getBookBySlug } from "@/lib/book";
import Link from "next/link";
import { useEffect, useState } from "react";

export default function BookDetails({ params }) {
  const [book, setBook] = useState(null);
  const { slug } = params;

  useEffect(() => {
    async function fetchBook() {
      try {
        const fetchedBook = await getBookBySlug(slug);
        setBook(fetchedBook);
      } catch (error) {
        console.error("Error fetching book:", error);
      }
    }

    fetchBook();
  }, [slug]);

  return (
    <div>
      <Header>
        <h1 className="text-xl font-bold">Book Details</h1>
      </Header>
      <div className="container mx-auto my-8">
        {book ? (
          <div className="max-w-md mx-auto bg-white rounded-lg overflow-hidden shadow-md">
            <div className="p-6">
              <h1 className="text-2xl font-bold mb-4">{book.title}</h1>
              <p className="text-gray-600 mb-2">
                <strong>Published Year:</strong> {book.publication_year}
              </p>
              <p className="font-bold mb-2">Authors:</p>
              <ul className="list-disc mb-4">
                {book.authors.map((author, i) => (
                  <span key={i}>
                    {author.name}
                    {i < book.authors.length - 1 && ", "}
                  </span>
                ))}
              </ul>
              <p className="font-bold mb-2">Summary:</p>
              <p className="text-gray-700 mb-4">{book.summary}</p>
              <Link
                href={"/"}
                className="bg-blue-500 text-white px-4 py-2 rounded hover:bg-blue-600"
              >
                Back to Book List
              </Link>
            </div>
          </div>
        ) : (
         <Loader/>
        )}
      </div>
    </div>
  );
}
