import BookEditForm from "@/app/components/BookEditForm";
import { Header } from "@/app/components/Header";

export default function UpdateBook({params}) {
  const {slug} = params

  return (
    <div>
      <Header>
        <h1 className="text-xl font-bold">Edit Book</h1>
      </Header>
      <div className="flex items-center justify-center">
      <BookEditForm slug={slug}/>
      </div>
    </div>
  );
}
