import BookCreateForm from "@/app/components/BookCreateForm";
import { Header } from "@/app/components/Header";


export default function AddBook() {
  return (
    <div>
      <Header>
        <h1 className="text-xl font-bold">Create New Book</h1>
      </Header>
      <div className="flex items-center justify-center">
      <BookCreateForm />
      </div>
    </div>
  );
}
