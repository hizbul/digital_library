"use client"
import { setSelectedAuthorOptions } from "@/provider/redux/features/authors/authorSlice";
import { fectAuthors } from "@/provider/redux/features/authors/authorsAction";
import { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import Select from 'react-select';


const TypeaheadSearch = () => {
  
  const authorOptions = useSelector((state) => state.authors.authorOptions)
  const selectedAuthorOptions = useSelector((state) => state.authors.selectedAuthorOptions)
  
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(fectAuthors())
  }, []); 

  const handleChange = (selected) => {
    dispatch(setSelectedAuthorOptions(selected))
  };

  return (
    <div>
      <Select
        value={selectedAuthorOptions}
        onChange={handleChange}
        options={authorOptions}
        placeholder="Search..."
        isMulti
      />
    </div>
  );
};

export default TypeaheadSearch;
