import {
  addAuthorFailure,
  setAuthorBio,
  setAuthorModalVisible,
  setAuthorName,
  setIsFormValid,
} from "@/provider/redux/features/authors/authorSlice";
import { addAuthor } from "@/provider/redux/features/authors/authorsAction";

import { useEffect, useRef } from "react";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";

export default function AuthorModal() {
  const modalRef = useRef(null);
  const author = useSelector((state) => state.authors.author);
  const errors = useSelector((state) => state.authors.errors);
  const isFormValid = useSelector((state) => state.authors.isFormValid);
  const authorModalVisible = useSelector(
    (state) => state.authors.authorModalVisible
  );

  const dispatch = useDispatch();
  useEffect(() => {
    if (!modalRef.current) {
      return;
    }
    if (authorModalVisible) {
      modalRef.current.showModal();
    } else {
      modalRef.current.close();
      dispatch(setAuthorModalVisible(false));
    }
  }, [authorModalVisible]);

  const handleClose = () => {
    modalRef.current.close();
    dispatch(setAuthorModalVisible(false));
    dispatch(setIsFormValid(false));
  };
  
  const handleESC = (event) => {
    event.preventDefault();
    handleClose();
  };

  useEffect(() => {
    validateForm();
  }, [author.name, author.bio]);

  // Validate form
  const validateForm = () => {
    let errors = {};

    if (!author.name) {
      errors.name = "Author Name is required.";
    }

    if (author.name && author.name.length > 100) {
      errors.name = "Name should be less than 100 char.";
    }

    dispatch(addAuthorFailure(errors));
    dispatch(setIsFormValid(Object.keys(errors).length === 0));
  };
  // Submit
  const handleSubmit = (e) => {
    e.preventDefault();
    if (isFormValid) {
      try {
        dispatch(addAuthor(author));

        if (Object.keys(errors).length === 0) {
          toast.success("Author created successfully!", {
            position: "top-right",
          });
          handleClose();
        } else {
          dispatch(addAuthorFailure(errors));
        }
      } catch (error) {
        console.error("Error creating author:", error);
      }
    } else {
      console.log("Form has errors. Please correct them.");
    }
  };

  return (
    <dialog
      ref={modalRef}
      id="author_create"
      onCancel={handleESC}
      className="modal modal-bottom sm:modal-middle"
    >
      <div className="modal-box bg-white border border-gray-300 rounded-md p-4 flex flex-col">
        <h3 className="font-bold text-lg mb-4">Create Author</h3>
        <form
          method="dialog"
          className="flex flex-col space-y-6"
          onSubmit={handleSubmit}
        >
          <div className="pb-2">
            <label
              htmlFor="name"
              className="block text-sm font-medium leading-6 text-gray-900 mb-2"
            >
              Author Name
            </label>
            <input
              type="text"
              name="name"
              id="name"
              autoComplete="name"
              onChange={(e) => dispatch(setAuthorName(e.target.value))}
              className={`w-full border-2 bg-transparent py-2.5 px-4 text-gray-900 focus:ring-2 focus:ring-indigo-600 rounded-md ${
                errors.name ? "border-red-300" : ""
              }`}
            />
            {errors.name && (
              <p className="text-red-500 mt-1 p-1 ">{errors.name}</p>
            )}
          </div>

          <div className="pb-2">
            <label
              htmlFor="bio"
              className="block text-sm font-medium leading-6 text-gray-900 mb-2"
            >
              Bio
            </label>
            <textarea
              id="bio"
              name="bio"
              rows={4}
              onChange={(e) => dispatch(setAuthorBio(e.target.value))}
              className="w-full border-2 bg-transparent py-2.5 px-4 text-gray-900 placeholder-gray-400 focus:ring-2 focus:ring-indigo-600 rounded-md"
              defaultValue={""}
            />
          </div>

          <div className="flex justify-end mt-4">
            <button className="btn btn-primary mr-2">Create Author</button>
            <button className="btn btn-default" onClick={handleClose}>
              Close
            </button>
          </div>
        </form>
      </div>
    </dialog>
  );
}
