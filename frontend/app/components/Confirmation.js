import { useEffect, useRef } from "react";

export default function Confirmation({
  message,
  visible,
  onConfirm,
  onCancel,
}) {
  const modalRef = useRef(null);

  useEffect(() => {
    if (!modalRef.current) {
      return;
    }
    visible ? modalRef.current.showModal() : modalRef.current.close();
  }, [visible]);

  const handleClose = () => {
    if (onClose) {
      onClose();
    }
  };

  const handleESC = (event) => {
    event.preventDefault();
    handleClose();
  };
  return (
    <dialog
      ref={modalRef}
      id="my_modal_1"
      className="modal"
      onCancel={handleESC}
    >
      <div className="modal-box">
        <h3 className="font-bold text-lg">{message}</h3>
        <div className="modal-action">
          <form method="dialog">
            <div className="flex-inline gap-2">
              <button className="btn btn-sm mr-2" onClick={onCancel}>
                Cancel
              </button>
              <button className="btn btn-sm btn-primary" onClick={onConfirm}>
                Confirm
              </button>
            </div>
          </form>
        </div>
      </div>
    </dialog>
  );
}
