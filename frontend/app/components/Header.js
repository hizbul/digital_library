import Link from "next/link";

export function Header({ children }) {
	return (
		<header className="p-2 sm:p-4 relative flex items-center justify-between bg-gray-100">
      <h1 className="font-medium text-3xl">
        <Link href={"/"}>
          Digital <span className="font-bold">Library</span>
        </Link>
      </h1>
      <div className="flex items-center">
        {children}
      </div>
    </header>
	);
}