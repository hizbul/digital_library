"use client";
import { deleteBook, fetchBooks } from "@/provider/redux/features/books/booksActions";
import {
  resetBooks,
  setDeletedBook,
  setNextPage,
  setShowDeleteConfirmation
} from "@/provider/redux/features/books/booksSlice";
import Link from "next/link";
import { useEffect } from "react";
import { CiEdit } from "react-icons/ci";
import { MdDelete } from "react-icons/md";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import Confirmation from "./Confirmation";
import Loader from "./Loader";


const BookList = () => {
  const dispatch = useDispatch();
  const books = useSelector((state) => state.books.books);
  const loading = useSelector((state) => state.books.loading);
  const error = useSelector((state) => state.books.error);
  const hasMoreData = useSelector((state) => state.books.hasMoreData);
  const page = useSelector((state) => state.books.page);
  const searchInput = useSelector((state) => state.books.searchInput);
  const showDeleteConfirmation = useSelector((state) => state.books.showDeleteConfirmation);
  const isDeleted = useSelector((state) => state.books.isDeleted);
  const bookToBeDeleted = useSelector((state) => state.books.bookToBeDeleted)



  useEffect(() => {
    let queryParams = "";
    if (searchInput) {
      queryParams = `?q=${searchInput}`;
      dispatch(resetBooks());
    }
    toast.error(error, {
      position: "top-right",
    });
    dispatch(fetchBooks(queryParams));
  }, [dispatch, searchInput]);

  const fetchMoreData = async () => {
    if (!hasMoreData || loading) return;

    let queryParams = `?page=${page + 1}`;
    dispatch(setNextPage());
    dispatch(fetchBooks(queryParams));
  };

  useEffect(() => {
    const handleScroll = () => {
      if (
        window.innerHeight + document.documentElement.scrollTop ===
          document.documentElement.offsetHeight &&
        hasMoreData
      ) {
        fetchMoreData();
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, [hasMoreData, fetchMoreData]);

  const handleDeleteBook = (book) => {
    dispatch(setShowDeleteConfirmation(true))
    dispatch(setDeletedBook(book))
  };

  const confirmDelete = () => {
    try {
      if (dispatch(deleteBook(bookToBeDeleted))) {
        toast.success("Book is deleted successfully!", {
          position: "top-right",
        });

      }
    } catch (error) {
      console.error("Error deleting book:", error);
    }

    dispatch(setShowDeleteConfirmation(false));
  };

  const confirmCancel = () => {
    dispatch(setShowDeleteConfirmation(false));
    dispatch(setDeletedBook({}))
  };


  return (
    <div className="overflow-x-auto">
      <table className="table">
        <thead>
          <tr>
            <th>Sr.</th>
            <th>Title</th>
            <th>Authors</th>
            <th>Summary</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {books.map((book, i) => (
            <tr key={i}>
              <td>{i + 1}</td>
              <td>
                <div className="flex items-center gap-3">
                  <div>
                    <div className="font-bold">
                      <Link href={`/books/${book.slug}/`}>{book.title}</Link>
                    </div>
                    <div className="text-sm opacity-50">
                      Published at {book.publication_year}
                    </div>
                  </div>
                </div>
              </td>
              <td>
                {book.authors.map((author, i) => (
                  <span key={i}>
                    {author.name}
                    {i < book.authors.length - 1 && ", "}
                  </span>
                ))}
              </td>
              <td>{book.summary.substring(0, 50) + "..."}</td>
              <td className="inline-flex gap-2 text-2xl">
                <Link href={`/books/edit/${book.slug}`}>
                <CiEdit
                  className="text-blue-400"
                  cursor="pointer"
                />
                </Link>
                
                <MdDelete
                  className="text-red-500"
                  cursor="pointer"
                  onClick={() => handleDeleteBook(book)}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      {showDeleteConfirmation && (
        <Confirmation
          message="Are you sure you want to delete this book?"
          visible={showDeleteConfirmation}
          onConfirm={confirmDelete}
          onCancel={confirmCancel}
        />
      )}
      {loading && (
        <Loader/>
      )}
      {!hasMoreData && books.length > 0 && <div className="text-center p-6 bg-info">No more book!</div>}
      {!hasMoreData && books.length < 1 && <div className="text-center p-6 bg-warning">No book found!</div>}
    </div>
  );
};

export default BookList;
