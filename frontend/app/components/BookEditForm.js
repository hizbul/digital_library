"use client";
import {
  setAuthorModalVisible
} from "@/provider/redux/features/authors/authorSlice";
import {
  getBookBySlug,
  updateBook,
} from "@/provider/redux/features/books/booksActions";

import {
  setBook,
  setErrors,
  setIsFormValid,
  setUpdateAbleBook,
} from "@/provider/redux/features/books/booksSlice";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { useEffect } from "react";
import { FaUser } from "react-icons/fa";
import { useDispatch, useSelector } from "react-redux";
import { toast } from "react-toastify";
import AuthorModal from "./AuthorModal";
import TypeaheadSearch from "./TypeaheadSearch";

const BookEditForm = ({ slug }) => {
  const dispatch = useDispatch();
  const updatedBook = useSelector((state) => state.books.updatedBook);
  const bookUpdatedSuccess = useSelector(
    (state) => state.books.bookUpdatedSuccess
  );
  const errors = useSelector((state) => state.books.errors);
  const error = useSelector((state) => state.books.error);
  const selectedAuthorOptions = useSelector(
    (state) => state.authors.selectedAuthorOptions
  );
  const router = useRouter()


  useEffect(() => {
    const fetchBookAndAuthors = async () => {
      await dispatch(getBookBySlug(slug));
    };

    fetchBookAndAuthors();
  }, [slug, updatedBook.title]);

  const handleChange = (e) => {
    const { name, value } = e.target;
    dispatch(
      setUpdateAbleBook({
        ...updatedBook,
        [name]: value,
      })
    );
  };

  const handleAuthorCreation = (e) => {
    e.preventDefault();
    dispatch(setAuthorModalVisible(true));
  };

  useEffect(() => {
    validateForm();
  }, [updatedBook]);

  // Validate form
  const validateForm = () => {
    let errors = {};

    if (!updatedBook.title) {
      errors.title = "Title is required.";
    }

    if (updatedBook.title && updatedBook.title.length > 100) {
      errors.title = "Title must be less than 100 char.";
    }

    if (!updatedBook.publication_year) {
      errors.publication_year = "Publication year is required.";
    }

    if (!updatedBook.summary) {
      errors.summary = "Summary is required.";
    }
    dispatch(setErrors(errors));
    dispatch(setIsFormValid(Object.keys(errors).length === 0));
  };

  const handleBookSubmit = async (e) => {
    e.preventDefault();
    const authors = selectedAuthorOptions.map((author) => author.value);
    dispatch(
      setBook({
        ...updatedBook,
        authors: authors,
      })
    );
    const response = await dispatch(updateBook(updatedBook, slug));

    if (response) {
      toast.success("Book updated successfully!", {
        position: "top-right",
      });
      router.push(`/books/${response.slug}`)
    } else if (error) {
      toast.error("An unexpected error occurred. Please try again later.", {
        position: "top-right",
      });
    }
  };

  return (
    <>
      <form
        onSubmit={handleBookSubmit}
        className="bg-white p-6 rounded-md w-full"
        style={{ width: 1000 }}
      >
        <div className="space-y-12 max-w-3xl">
          <div className="border-gray-900/10 pb-6">
            <div className="mt-10 grid grid-cols-1 gap-x-6 gap-y-8 sm:grid-cols-6">
              <div className="sm:col-span-4">
                <label
                  htmlFor="title"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Title
                </label>
                <div className="mt-2">
                  <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 ">
                    <input
                      type="text"
                      name="title"
                      value={updatedBook.title}
                      onChange={handleChange}
                      className={`block flex-1 border-2 bg-transparent rounded-md py-1.5 pl-1 text-gray-900 focus:ring-0 sm:text-sm sm:leading-6 ${
                        errors.title ? "border-red-300" : ""
                      }`}
                    />
                  </div>
                  {errors.title && (
                    <p className="text-red-500 mt-1 p-1 ">{errors.title}</p>
                  )}
                </div>
              </div>
              <div className="sm:col-span-4 flex flex-col items-start">
                <label
                  htmlFor="author"
                  className="block text-sm font-medium leading-6 text-gray-900 mb-2"
                >
                  Authors
                </label>
                <div className="flex w-full">
                  <div className="flex-grow pr-2">
                    <TypeaheadSearch />
                  </div>
                  <button
                    className="btn btn-outline btn-info btn-sm"
                    onClick={handleAuthorCreation}
                  >
                    <FaUser />
                    New Author
                  </button>
                </div>
              </div>
              <div className="sm:col-span-4">
                <label
                  htmlFor="publication_year"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Publication Year
                </label>
                <div className="mt-2">
                  <div className="flex rounded-md shadow-sm ring-1 ring-inset ring-gray-300 focus-within:ring-2 focus-within:ring-inset focus-within:ring-indigo-600 ">
                    <input
                      type="number"
                      name="publication_year"
                      min={1000}
                      max={2024}
                      value={updatedBook.publication_year || ""}
                      onChange={handleChange}
                      className={`block flex-1 rounded-md border-2 bg-transparent py-1.5 pl-1 text-gray-900 placeholder:text-gray-400 focus:ring-0 sm:text-sm sm:leading-6 ${
                        errors.publication_year ? "border-red-300" : ""
                      }`}
                    />
                  </div>
                  {errors.publication_year && (
                    <p className="text-red-500 mt-1 p-1 ">
                      {errors.publication_year}
                    </p>
                  )}
                </div>
              </div>

              <div className="col-span-full">
                <label
                  htmlFor="about"
                  className="block text-sm font-medium leading-6 text-gray-900"
                >
                  Summary
                </label>
                <div className="mt-2">
                  <textarea
                    name="summary"
                    value={updatedBook.summary}
                    rows={5}
                    className={`block p-2 w-full rounded-md border-2 py-1.5 text-gray-900 shadow-sm ring-1 ring-inset ring-gray-300 placeholder:text-gray-400 focus:ring-2 focus:ring-inset focus:ring-indigo-600 sm:text-sm sm:leading-6 ${
                      errors.summary ? "border-red-300" : ""
                    }`}
                    onChange={handleChange}
                  />
                </div>
                {errors.summary && (
                  <p className="text-red-500 mt-1 p-1 ">{errors.summary}</p>
                )}
              </div>
            </div>
          </div>
          <div className="flex justify-end">
            <button className="btn btn-primary mr-2">Update Book</button>
            <Link href={"/"} className="btn">
              Close
            </Link>
          </div>
        </div>
      </form>
      <AuthorModal />
    </>
  );
};

export default BookEditForm;
