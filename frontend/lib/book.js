// lib/api.js
import { BASE_URL, HEADERS } from "@/utils/constants";
import { notFound } from "next/navigation";


export const getBookBySlug = async (slug) => {
  try {
    const res = await fetch(`${BASE_URL}/books/${slug}/`, {
      headers: HEADERS
    });
    if (res.status === 404) {
      notFound()
    }
    const response = await res.json();
    return response
  } catch (error) {
    return {error: "Error fetching book:", error};
  }
};