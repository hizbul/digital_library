#!/bin/sh

echo DEBUG=0 >> .env
echo SECRET_KEY=$SECRET_KEY >> .env
echo DJANGO_ALLOWED_HOSTS=$DJANGO_ALLOWED_HOSTS >> .env
echo CORS_ALLOWED_ORIGINS=$CORS_ALLOWED_ORIGINS >> .env
echo env.prodIRONMENT=production >> .env
echo DJANGO_SETTINGS_MODULE=config.settings.production >> .env
echo SQL_ENGINE=django.db.backends.postgresql >> .env
echo DATABASE=postgres >> .env
echo SQL_DATABASE=$SQL_DATABASE >> .env
echo SQL_USER=$SQL_USER >> .env
echo SQL_PASSWORD=$SQL_PASSWORD >> .env
echo SQL_HOST=$SQL_HOST >> .env
echo SQL_PORT=$SQL_PORT >> .env
echo CELERY_BROKER_URL=amqp://broker:5672// >> .env
echo REDIS_URL = redis://result:6379/0 >> .env
echo API_HEADER=digital_library_header_from_api >> .env
echo SENTRY_DSN=https://cb79ce1f2d738100d70f9228e5bea0fc@o479814.ingest.sentry.io/4506642033606656 >> .env

echo API_BASE_URL=$API_BASE_URL >> .env
echo HEADER_KEY='X-Api-Required-Header' >> .env
echo HEADER_SECURITY=digital_library_header_from_api >> .env


echo POSTGRES_USER=$SQL_USER >> backend/.env.prod.db
echo POSTGRES_PASSWORD=$SQL_PASSWORD >> backend/.env.prod.db
echo POSTGRES_DB=$SQL_DATABASE >> backend/.env.prod.db